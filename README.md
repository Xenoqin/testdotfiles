# testDotfiles
For random temp storage of dotfiles and other assorted things


# Dependancies

AWM \
Ranger \
Neofetch \
Nitrogen \
Pywal (sudo apt install imagemagick: required for it to work [for debian based distros]) 

# Notes

Nitrogen uses scaled-fill \
Nitrogen has to have drawn a WP for it to auto draw 


https://www.youtube.com/watch?v=pz8Jbxr3LE8 \
https://www.youtube.com/watch?v=qrGc_anbodU \
https://gitlab.com/dwt1/dotfiles \
https://github.com/ChrisTitusTech/titus-awesome \
https://wiki.archlinux.org/title/Cursor_themes \
https://www.pling.com/p/1360254 

https://www.curseforge.com/minecraft/mc-mods/presence-footsteps \
https://www.curseforge.com/minecraft/mc-mods/sound-physics-remastered \
https://www.curseforge.com/minecraft/mc-mods/falling-leaves-fabric \
https://www.curseforge.com/minecraft/mc-mods/mouse-wheelie \
https://www.curseforge.com/minecraft/mc-mods/illuminations \
https://illuminations.uuid.gg/register \
https://www.curseforge.com/minecraft/mc-mods/nullscape-end-reborn \
https://www.curseforge.com/minecraft/mc-mods/distant-horizons \
https://www.curseforge.com/minecraft/mc-mods/terralith \
https://www.curseforge.com/minecraft/mc-mods/amplified-nether 

https://www.razer.com/shop/respawn/respawn-by-razer?_ga=2.62685932.1360611800.1603032857-396282814.1595352276&irclickid=xa0z6wRwaxyIRSi0nTTQA2nlUkGRBfxlRVdj340&irgwc=1&utm_source=ir&utm_medium=affiliate&utm_content=1266515&utm_term=Hardware%20Canucks 
